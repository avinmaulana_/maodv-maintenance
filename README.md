# maodv-maintenance

AODV - MANET - Modified Route Maintenance
Nama  : Avin Maulana
NRP   : 5111850010029
Kelas : A

Improvement pada AODV yang dilakukan dengan modifikasi pada Route Maintenance.
Penambahan parameter baru pada Halo Packet, yaitu Overhearing Factor (OF) dan
Height sebagai penentu untuk penentuan Route Maintenance ketika terjadi link
break / route break.