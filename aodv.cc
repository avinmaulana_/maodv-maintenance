/*
Copyright (c) 1997, 1998 Carnegie Mellon University.  All Rights
Reserved. 

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The AODV code developed by the CMU/MONARCH group was optimized and tuned by Samir Das and Mahesh Marina, University of Cincinnati. The work was partially done in Sun Microsystems. Modified for gratuitous replies by Anant Utgikar, 09/16/02.

*/

//#include <ip.h>

#include <aodv/aodv.h>
#include <aodv/aodv_packet.h>
#include <random.h>
#include <cmu-trace.h>
//#include <energy-model.h>

#define max(a,b)        ( (a) > (b) ? (a) : (b) )
#define CURRENT_TIME    Scheduler::instance().clock()

//#define DEBUG
//#define ERROR

#ifdef DEBUG
static int route_request = 0;
#endif


/*
  TCL Hooks
*/


int hdr_aodv::offset_;
static class AODVHeaderClass : public PacketHeaderClass {
public:
        AODVHeaderClass() : PacketHeaderClass("PacketHeader/AODV",
                                              sizeof(hdr_all_aodv)) {
	  bind_offset(&hdr_aodv::offset_);
	} 
} class_rtProtoAODV_hdr;

static class AODVclass : public TclClass {
public:
        AODVclass() : TclClass("Agent/AODV") {}
        TclObject* create(int argc, const char*const* argv) {
          assert(argc == 5);
          //return (new AODV((nsaddr_t) atoi(argv[4])));
	  return (new AODV((nsaddr_t) Address::instance().str2addr(argv[4])));
        }
} class_rtProtoAODV;


int
AODV::command(int argc, const char*const* argv) {
  if(argc == 2) {
  Tcl& tcl = Tcl::instance();
    
    if(strncasecmp(argv[1], "id", 2) == 0) {
      tcl.resultf("%d", index);
      return TCL_OK;
    }
    
    if(strncasecmp(argv[1], "start", 2) == 0) {
      btimer.handle((Event*) 0);

//#ifndef AODV_LINK_LAYER_DETECTION		//Modifikasi: Enable Hello
      htimer.handle((Event*) 0);
      ntimer.handle((Event*) 0);
//#endif // LINK LAYER DETECTION		//Modifikasi: Enable Hello

      rtimer.handle((Event*) 0);
      return TCL_OK;
     }               
  }
  else if(argc == 3) {
    if(strcmp(argv[1], "index") == 0) {
      index = atoi(argv[2]);
      return TCL_OK;
    }
/* Modifikasi : Enabling Prominiscius Mode*/
else if (strcmp(argv[1], "install-tap") == 0) {
mac_ = (Mac*)TclObject::lookup(argv[2]);
if (mac_ == 0) return TCL_ERROR;
mac_->installTap(this);
return TCL_OK;
}
/******************************************/
    else if(strcmp(argv[1], "log-target") == 0 || strcmp(argv[1], "tracetarget") == 0) {
      logtarget = (Trace*) TclObject::lookup(argv[2]);
      if(logtarget == 0)
	return TCL_ERROR;
      return TCL_OK;
    }
    else if(strcmp(argv[1], "drop-target") == 0) {
    int stat = rqueue.command(argc,argv);
      if (stat != TCL_OK) return stat;
      return Agent::command(argc, argv);
    }
    else if(strcmp(argv[1], "if-queue") == 0) {
    ifqueue = (PriQueue*) TclObject::lookup(argv[2]);
      
      if(ifqueue == 0)
	return TCL_ERROR;
      return TCL_OK;
    }
    else if (strcmp(argv[1], "port-dmux") == 0) {
    	dmux_ = (PortClassifier *)TclObject::lookup(argv[2]);
	if (dmux_ == 0) {
		fprintf (stderr, "%s: %s lookup of %s failed\n", __FILE__,
		argv[1], argv[2]);
		return TCL_ERROR;
	}
	return TCL_OK;
    }
  }
  return Agent::command(argc, argv);
}
/* Modifikasi : Overhearing Factor (OF) */
void
AODV::tap(const Packet *p) {

	if(rtable.head()) return;

	FILE * ovhrdFile;
	bool add = true;	
	ovhrdFile = fopen("overhearing.txt", "a"); 
	fprintf(ovhrdFile, "\n prom mode: my  Address is =%d", index);
	struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *ih = HDR_IP(p);
       	if(ch->ptype() == PT_AODV) {
		fprintf(ovhrdFile, "\toverheard an AODV packet: ");
	     
	        struct hdr_aodv *ah = HDR_AODV(p);
		struct hdr_aodv_reply *rp = HDR_AODV_REPLY(p);		
 
 		switch(ah->ah_type) {

 		case AODVTYPE_RREQ:
	   	fprintf(ovhrdFile, "\tRREQ");
		break;

 		case AODVTYPE_RREP:
   		fprintf(ovhrdFile, "\tRREP");
		aodv_art_entry *art;
		
// Modifikasi : Tambahan untuk OF
		for (art=artable.head();art; art = art->art_link.le_next)
		{
			if(art->art_nexthop == ch->prev_hop_) //rp->rp_dst)
			{
				art->art_height = ch->height;
				add = false;
				break;
			}
		}
		if(add && ch->prev_hop_ != index)
		{
			printf("\n destination field added in art: %i", ih->daddr());
			artable.art_add(ch->prev_hop_, rp->rp_dst, ch->height);//rp->rp_dst);
		}
//----------------------------------------------------------------------------	
   		break;

 		case AODVTYPE_RERR:
   		fprintf(ovhrdFile, "\tRERR");
   		break;

 		case AODVTYPE_HELLO:	
		fprintf(ovhrdFile, "\tHELLO overheard from _%i_ with OF %d", rp->rp_dst , rp->rp_OF);				
   		break;
        
 		default:
   		fprintf(ovhrdFile, "\tInvalid AODV type (%x)", ah->ah_type);
   		exit(1);
		}
	} 

//------------------------------------Modifikasi : Tambahan untuk OF------------------------------------------
  	if(DATA_PACKET(ch->ptype())) 
	{
		//struct hdr_ip *ih = HDR_IP(p);
		aodv_art_entry *art;		
		//nHeight = ch->height;			
	 	fprintf(ovhrdFile, "\toverheard a data packet from _%i_ to _%i_", ch->prev_hop_, ch->next_hop_);

		for (art=artable.head();art; art = art->art_link.le_next)
		{
			if(art->art_nexthop == ch->prev_hop_)//ih->daddr())
			{
				art->art_height = ch->height;
				add = false;
				break;
			}
		}
		if(add && (ch->next_hop_ != index) )// && (ch->ch_height >= 0) && (ch->ch_height >= nHeight))
		{
			//printf("\n destination field added in art: %i", ih->daddr());
			artable.art_add(ch->prev_hop_, ih->daddr(), ch->height);
		}			
	}
//----------------------------------------------------------------------------------		
	//art_print(index);	commenting out
	fclose(ovhrdFile);
}

 
AODV_Neighbor* 
AODV::fetchNeighbor()
{

//Cek nilai OF
//iJika lebih besar atau sama dengan 2
//Dibandingkan dengan neighbor table
//Update return_nb dengan neighbor baru JIKA height-nya lebih besar atau sama dengan height upstream node

//printf("\n Inside fetchNeighbor at %f \n", CURRENT_TIME);
	AODV_Neighbor *nb = nbhead.lh_first;
	AODV_Neighbor *return_nb = nbhead.lh_first;
	//if(nbhead.lh_first)	//checking whether the neighbor list is not empty
	//{
		for(; nb; nb = nb->nb_link.le_next)
		{
//printf("\n Inside fetchNeighbor (for) at %f \n", CURRENT_TIME);
			if(nb->nb_link.le_next)
			{
//printf("\n Inside fetchNeighbor if(1) at %f \n", CURRENT_TIME);
				if((nb->nb_link.le_next->nb_OF > nb->nb_OF) && (nb->nb_link.le_next->nb_height > nHeight))
				{
//printf("\n Inside fetchNeighbor if(2) at %f \n", CURRENT_TIME);
					if((nb->nb_link.le_next->nb_OF > return_nb->nb_OF) && (nb->nb_link.le_next->nb_addr != index))
					{
//printf("\n Inside fetchNeighbor if(3) at %f \n", CURRENT_TIME);
						return_nb = nb->nb_link.le_next;  //jika OF dari next lebih besar daru current dan stored
					}
				}
				else if((nb->nb_OF > return_nb->nb_OF) && (nb->nb_addr != index) && (nb->nb_height > nHeight)) // atau jika OF dari current lebih besar dari stored
				{
//printf("\n Inside fetchNeighbor else if at %f \n", CURRENT_TIME);
					return_nb = nb;
				}
			}		
		}
	//}
	if(return_nb)
	{
		if((return_nb->nb_OF > 1) && (return_nb->nb_height > nHeight))	//neighbor dengan OF paling tidak = 2...
		{
//printf("\n Inside fetchNeighbor if(4) at %f \n", CURRENT_TIME);
			return return_nb;
		}
		else				//neighbor = NULL jika tidak memenuhi
		{
//printf("\n Inside fetchNeighbor else at %f \n", CURRENT_TIME);
			return NULL;
		}
	}
#ifdef DEBUG
FILE * stderr;
stderr = fopen("debugging.txt", "a");
fprintf(stderr," fetchNeighbor berjalan, hasil return_nb = %d\n", return_nb);
fclose(stderr);
#endif  
}

aodv_rt_entry* AODV::findNeighbor(nsaddr_t id)
{
//printf("\n Inside findNeighbor \n");
	aodv_rt_entry *rt;
	for(rt = rtable.head(); rt; rt = rt->rt_link.le_next)
	{
		if(rt->rt_nexthop == id)
		{
			return rt;
		}
	}
	return NULL;
}
	
/***********************************************/

 
/* 
   Constructor
*/

AODV::AODV(nsaddr_t id) : Agent(PT_AODV),
			  btimer(this), htimer(this), ntimer(this), 
			  rtimer(this), lrtimer(this), rqueue() {
 
                
  index = id;
  seqno = 2;
  bid = 1;

  LIST_INIT(&nbhead);
  LIST_INIT(&bihead);

  logtarget = 0;
  ifqueue = 0;

// Modifikasi: Tambahan untuk OF
  OF = 1;
  dFlag = false;
  nHeight = -2;
}

/*
  Timers
*/

void
BroadcastTimer::handle(Event*) {
  agent->id_purge();
  Scheduler::instance().schedule(this, &intr, BCAST_ID_SAVE);
}

void
HelloTimer::handle(Event*) {
   if(agent->rtable.head() || agent->artable.head()){	// Modifikasi : Hanya Rute Aktif yang bisa menggunakan Hello 
	agent->sendHello();
   }		
   double interval = MinHelloInterval + 
                 ((MaxHelloInterval - MinHelloInterval) * Random::uniform());

/*------------------ Modifikasi : Hello Dinamis  ------------------------------
	u_int8_t numEntries = agent->nb_count();
	if(numEntries)
	{
		if(numEntries > 6)
		{
			numEntries = 6;
		}
		//printf("\n I am %i, my neighbor count = %i at %.4lf", agent->index, numEntries, CURRENT_TIME);
		interval = interval / numEntries;
		//printf("\n dynamic interval calculated = %.4lf", interval);
	}
//--------------------------------------------------------------------*/

   assert(interval >= 0);
   Scheduler::instance().schedule(this, &intr, interval);
}

void
NeighborTimer::handle(Event*) {
  agent->nb_purge();
  agent->art_purge();	// Modifikasi : Tambahan untuk OF
  Scheduler::instance().schedule(this, &intr, HELLO_INTERVAL);
}

void
RouteCacheTimer::handle(Event*) {
  agent->rt_purge();
#define FREQUENCY 0.5 // sec
  Scheduler::instance().schedule(this, &intr, FREQUENCY);
}

void
LocalRepairTimer::handle(Event* p)  {  // SRD: 5/4/99
aodv_rt_entry *rt;
struct hdr_ip *ih = HDR_IP( (Packet *)p);

   /* you get here after the timeout in a local repair attempt */
   /*	fprintf(stderr, "%s\n", __FUNCTION__); */


    rt = agent->rtable.rt_lookup(ih->daddr());
	
    if (rt && rt->rt_flags != RTF_UP) {
    // route is yet to be repaired
    // I will be conservative and bring down the route
    // and send route errors upstream.
    /* The following assert fails, not sure why */
    /* assert (rt->rt_flags == RTF_IN_REPAIR); */
		
      //rt->rt_seqno++;
      agent->rt_down(rt);
      // send RERR    
    }
    Packet::free((Packet *)p);
}


/*
   Broadcast ID Management  Functions
*/


void
AODV::id_insert(nsaddr_t id, u_int32_t bid) {
BroadcastID *b = new BroadcastID(id, bid);

 assert(b);
 b->expire = CURRENT_TIME + BCAST_ID_SAVE;
 LIST_INSERT_HEAD(&bihead, b, link);
}

/* SRD */
bool
AODV::id_lookup(nsaddr_t id, u_int32_t bid) {
BroadcastID *b = bihead.lh_first;
 
 // Search the list for a match of source and bid
 for( ; b; b = b->link.le_next) {
   if ((b->src == id) && (b->id == bid))
     return true;     
 }
 return false;
}

void
AODV::id_purge() {
BroadcastID *b = bihead.lh_first;
BroadcastID *bn;
double now = CURRENT_TIME;

 for(; b; b = bn) {
   bn = b->link.le_next;
   if(b->expire <= now) {
     LIST_REMOVE(b,link);
     delete b;
   }
 }
}

/*
  Helper Functions
*/

double
AODV::PerHopTime(aodv_rt_entry *rt) {
int num_non_zero = 0, i;
double total_latency = 0.0;

 if (!rt)
   return ((double) NODE_TRAVERSAL_TIME );
	
 for (i=0; i < MAX_HISTORY; i++) {
   if (rt->rt_disc_latency[i] > 0.0) {
      num_non_zero++;
      total_latency += rt->rt_disc_latency[i];
   }
 }
 if (num_non_zero > 0)
   return(total_latency / (double) num_non_zero);
 else
   return((double) NODE_TRAVERSAL_TIME);

}

/*
  Modifikasi: Saat Terjadi Link Failure
*/

static void
aodv_rt_failed_callback(Packet *p, void *arg) {
//printf("\n Inside callback at %f  +++++++++++++++++++++++++++++++++++++++", CURRENT_TIME);
  ((AODV*) arg)->rt_ll_failed(p);
}

/*
 * This routine is invoked when the link-layer reports a route failed.
 */
void
AODV::rt_ll_failed(Packet *p) {	
printf("\n rt_ll_failed pada %f Node: %i", CURRENT_TIME, index);	
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *ih = HDR_IP(p);
aodv_rt_entry *rt;
nsaddr_t broken_nbr = ch->next_hop_;
bool toDelete = true;	//Modifikasi: Flag untuk cek

#ifndef AODV_LINK_LAYER_DETECTION
 drop(p, DROP_RTR_MAC_CALLBACK);
#else 

 /*
  * Non-data packets and Broadcast Packets can be dropped.
  */
  if(! DATA_PACKET(ch->ptype()) ||
     (u_int32_t) ih->daddr() == IP_BROADCAST) {
    drop(p, DROP_RTR_MAC_CALLBACK);
    return;
  }
  log_link_broke(p);
	if((rt = rtable.rt_lookup(ih->daddr())) == 0) {
    drop(p, DROP_RTR_MAC_CALLBACK);
    return;
  }
  log_link_del(ch->next_hop_);	

 // if (ch->num_forwards() > rt->rt_hops) {	//Modifikasi: Local Repair Condition
	AODV_Neighbor* nb;			//Modifikasi: Tambahan untuk OF
	//printf("Inside rt_ll_failed: ch->prev_hop_ = %i, index = %i============================================\n", ch->prev_hop_, index);
	if((nb = fetchNeighbor())!= NULL)	// Modifikasi: Menentukan Neighbor (tetangga) dengan OF tertinggi
  	{
//printf("\n Inside if(nb = fetchNeighbor()) \n");
		// to be added
		aodv_rt_entry *rt = findNeighbor(broken_nbr);
		if(!rt)
		{
			//printf("\n findNeighbor returns NULL\n");
			//return;	//its working!!
	
		}
		else
		{
printf("\n At %f I am: %i\t", CURRENT_TIME, index);
printf("the prev node is: %i\t", ch->prev_hop_);
printf("the old next node is: %i \t", rt->rt_nexthop);
			rt->rt_nexthop = nb->nb_addr;
printf("the new next node is: %i ", rt->rt_nexthop);
//printf(" \tthe flag is: %d", rt->rt_flags);
//printf("\n the old expire timer: %.4lf", rt->rt_expire);
rt_print(index);
			toDelete = false;				
		}
		// Buffer the packet 
  		rqueue.enque(p);
		return;
  	}

#ifdef AODV_LOCAL_REPAIR
  /* if the broken link is closer to the dest than source, 
     attempt a local repair. Otherwise, bring down the route. */


  if (ch->num_forwards() > rt->rt_hops) {
    local_rt_repair(rt, p); // local repair
    // retrieve all the packets in the ifq using this link,
    // queue the packets for which local repair is done, 
    return;
  }
  else	
#endif // LOCAL REPAIR	

  {
    drop(p, DROP_RTR_MAC_CALLBACK);
    // Do the same thing for other packets in the interface queue using the
    // broken link -Mahesh
while((p = ifqueue->filter(broken_nbr))) {
     drop(p, DROP_RTR_MAC_CALLBACK);
    }
    //if(toDelete)	//
    //{
//printf("\n Inside if(toDelete), calling nb_delete at %f ++++++------------+++++++-+_=-+-+-=", CURRENT_TIME);
	
    	//nb_delete(broken_nbr);
    	//art_delete(broken_nbr);		//
    //}
  }

#endif // LINK LAYER DETECTION
}

void
AODV::handle_link_failure(nsaddr_t id) {
//printf("Inside handle_link_failure at %f I am %i *******************************************", CURRENT_TIME, index);
aodv_rt_entry *rt, *rtn;
Packet *rerr = Packet::alloc();
#ifdef DEBUG
FILE * stderr;
stderr = fopen("debugging.txt", "a");
#endif
struct hdr_aodv_error *re = HDR_AODV_ERROR(rerr);

 re->DestCount = 0;
 for(rt = rtable.head(); rt; rt = rtn) {  // for each rt entry
   rtn = rt->rt_link.le_next; 
   if ((rt->rt_hops != INFINITY2) && (rt->rt_nexthop == id) ) {
     assert (rt->rt_flags == RTF_UP);
     assert((rt->rt_seqno%2) == 0);
     rt->rt_seqno++;
     re->unreachable_dst[re->DestCount] = rt->rt_dst;
     re->unreachable_dst_seqno[re->DestCount] = rt->rt_seqno;
#ifdef DEBUG
     fprintf(stderr, "%s(%f): %d\t(%d\t%u\t%d)\n", __FUNCTION__, CURRENT_TIME,
		     index, re->unreachable_dst[re->DestCount],
		     re->unreachable_dst_seqno[re->DestCount], rt->rt_nexthop);
#endif // DEBUG
     re->DestCount += 1;
     rt_down(rt);
   }
   // remove the lost neighbor from all the precursor lists
   rt->pc_delete(id);
 }   

 if (re->DestCount > 0) {
#ifdef DEBUG
   fprintf(stderr, "%s(%f): %d\tsending RERR...\n", __FUNCTION__, CURRENT_TIME, index);
   fclose(stderr);
#endif // DEBUG
//printf("\nsending RERR...at time = %f, I am = %i >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.." ,CURRENT_TIME, index);
   sendError(rerr, false);
 }
 else { 
   Packet::free(rerr);
 }
}

void
AODV::local_rt_repair(aodv_rt_entry *rt, Packet *p) {
//printf("\n inside local_rt_repair \n");
#ifdef DEBUG
FILE * stderr;
stderr = fopen("debugging.txt", "a");
fprintf(stderr,"%s: Dst - %d\n", __FUNCTION__, rt->rt_dst);
fclose(stderr);
#endif  
  // Buffer the packet 
  rqueue.enque(p);

  // mark the route as under repair 
  rt->rt_flags = RTF_IN_REPAIR;

	//printf(" \n Inside local_rt_repair at %.4lf, going to call sendRequest...", CURRENT_TIME);	//
  	sendRequest(rt->rt_dst);

  // set up a timer interrupt
  Scheduler::instance().schedule(&lrtimer, p->copy(), rt->rt_req_timeout);
}

void
AODV::rt_update(aodv_rt_entry *rt, u_int32_t seqnum, u_int16_t metric,
	       	nsaddr_t nexthop, double expire_time) {

     rt->rt_seqno = seqnum;
     rt->rt_hops = metric;
     rt->rt_flags = RTF_UP;
     rt->rt_nexthop = nexthop;
     rt->rt_expire = expire_time;
}

void
AODV::rt_down(aodv_rt_entry *rt) {
  /*
   *  Make sure that you don't "down" a route more than once.
   */

  if(rt->rt_flags == RTF_DOWN) {
    return;
  }

  // assert (rt->rt_seqno%2); // is the seqno odd?
  rt->rt_last_hop_count = rt->rt_hops;
  rt->rt_hops = INFINITY2;
  rt->rt_flags = RTF_DOWN;
  rt->rt_nexthop = 0;
  rt->rt_expire = 0;

} /* rt_down function */

/*
  Route Handling Functions
*/
void  AODV::nb_print(nsaddr_t node_id) {
	    FILE * dumpFile;
	    AODV_Neighbor *nb = nbhead.lh_first;	
	 
	    dumpFile = fopen("nbtable.txt", "a");
	if(nb){	   
 
	    fprintf(dumpFile, "==========================================================\n");
	    fprintf(dumpFile, "Node_id | cur_time | Neigh | Expiry |  nb->OF | nb->height \n");	}
//if(nb){	 printf( "Neigbor entries:%i\t %.4lf\t\t %i\t %.4lf  %i\n", node_id, CURRENT_TIME, nb->nb_addr, nb->nb_expire, nb->nb_OF);}
	   for(; nb; nb = nb->nb_link.le_next){	 
	        fprintf(dumpFile, "%i\t %.4lf\t\t %i\t %.4lf  %i \t %.1lf\n", node_id, CURRENT_TIME, nb->nb_addr, nb->nb_expire, nb->nb_OF, nb->nb_height);	 
	    }
	 
	    fclose(dumpFile);
}	// Modifikasi: Print Neighbor Table


void  AODV::rt_print(nsaddr_t node_id) {	// Modifikasi: Tambah fungsi untuk print Routing Table
	    FILE * dumpFile;
	    //char dumpFileName[50] = "rtable.txt";
	   // const char* dumpFileName = new char[15];
	    //dumpFileName = "rtable.txt";
	 
	    dumpFile = fopen("rtable.txt", "a");
	 
	    aodv_rt_entry *rt;

if(rtable.head())
{	 
	    fprintf(dumpFile, "==============================================================================     \t=====================\n");
	    fprintf(dumpFile, "Node_id | cur_time | Dest_id | nextHop | totHops | seqNo | expireTime | Flags |    \tLifetime | OF | ht\n");
	 
	    for (rt=rtable.head();rt; rt = rt->rt_link.le_next) {
	 
	        fprintf(dumpFile, "%i\t %.4lf\t\t %i\t %i\t %i\t    %i\t %.4lf   \t%d \t\t%.4lf   %i  /* %.4lf*/\n", node_id, CURRENT_TIME, rt->rt_dst, rt->rt_nexthop, rt->rt_hops, rt->rt_seqno, rt->rt_expire, rt->rt_flags, rt->rt_expire - CURRENT_TIME, OF, nHeight);
	 
	    }
}	 
	    fclose(dumpFile);
}	//


void  AODV::art_print(nsaddr_t node_id) {	// Modifikasi: Printing Alternate Route Table (ART)
	    FILE * dumpFile;
	    //char dumpFileName[50] = "rtable.txt";
	   // const char* dumpFileName = new char[15];
	    //dumpFileName = "rtable.txt";
	 
	    dumpFile = fopen("ARtable.txt", "a");
	 
	    aodv_art_entry *art;
if(artable.head())
	{	 
	    fprintf(dumpFile, "============================================================\n");
	    fprintf(dumpFile, "Node_id | cur_time | Dest_id | nextHop | height | expireTime \n");
	 
	    for (art=artable.head();art; art = art->art_link.le_next) 
	    {
	 
	        fprintf(dumpFile, "%i\t %.4lf\t\t %i\t %i\t %.1lf\t %.3lf\n", node_id, CURRENT_TIME, art->art_dst, art->art_nexthop, art->art_height, art->art_expire);
	    }
	}//if	 
fclose(dumpFile);
}	//


void
AODV::rt_resolve(Packet *p) {
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *ih = HDR_IP(p);
aodv_rt_entry *rt;
aodv_art_entry *art;	//Modifikasi : Tambahan untuk OF
AODV_Neighbor *nb;	//Modifikasi : Tambahan untuk OF
 /*
  *  Set the transmit failure callback.  That
  *  won't change.
  */
 ch->xmit_failure_ = aodv_rt_failed_callback;
 ch->xmit_failure_data_ = (void*) this;
//if((CURRENT_TIME > 6.0) && (index >= 4)) {
//printf("\n -----------inside rt_resolve at %.4lf, I am %i,ih->saddr()= %i ", CURRENT_TIME, index, ih->saddr());	}//
#ifdef DEBUG
FILE * stderr;
stderr = fopen("debugging.txt", "a");
#endif
	rt = rtable.rt_lookup(ih->daddr());
 if(rt == 0) {
	  rt = rtable.rt_add(ih->daddr());
	
		//--------------------------- Modifikasi : Kondisi untuk Neighbor Terpilih --------------------
    //--------------------------- Proses mencari dan memilih Neighbor dengan Height tertinggi --------------------

		if(artable.head())
		{
 			for(art=artable.head(); art; art = art->art_link.le_next)
			{
				if((art->art_nexthop != ch->prev_hop_) )//&& (art->art_height > nHeight))
				{
					printf("\n inside rt_resolve at %.4lf, I am %i,if-for-if \t", CURRENT_TIME, index);
					printf(" art->art_nexthop = %i", art->art_nexthop);
					printf("\t ch->prev_hop_ = %i", ch->prev_hop_);
					nb = nb_lookup(art->art_nexthop);
					if(nb)
					{
						rt_update(rt, nb->nb_seqno, 1, art->art_nexthop, CURRENT_TIME);
						printf("\n-----------updated the rt entry, inside rt_resolve\n");
					}
					//break;
				}
			}
		}					
		//------------------------------------------------------------------------------------		
 }

 /*
  * If the route is up, forward the packet 
  */
	
 if(rt->rt_flags == RTF_UP) {
   assert(rt->rt_hops != INFINITY2);
//if((CURRENT_TIME > 3.5) && (index == 12)) {
//printf("\n -----------inside rt_resolve: if(rt->rt_flags == RTF_UP) at %.4lf, I am %i", CURRENT_TIME, index);}

//------------------------------------Modifikasi : Increment nilai Height ------------------------------------------
if(ch->ptype() == PT_CBR) { nHeight = ch->height + 1; ch->height++; } 
ch->prev_hop_ = index; 	// Modifikasi : Update prev_hop_ dari hdr_cmn untuk data packets
   forward(rt, p, NO_DELAY);
 }
 /*
  *  if I am the source of the packet, then do a Route Request.
  */
	else if(ih->saddr() == index) {	
   rqueue.enque(p);
//if((CURRENT_TIME > 3.5) && (index == 12)) {
//printf(" \n Inside rt_resolve() at %.4lf, going to call sendRequest...", CURRENT_TIME);	}
   sendRequest(rt->rt_dst);	
 }
 /*
  *	A local repair is in progress. Buffer the packet. 
  */
 else if (rt->rt_flags == RTF_IN_REPAIR) {
   rqueue.enque(p);
 }

 /*
  * I am trying to forward a packet for someone else to which
  * I don't have a route.
  */
 else {
//if((CURRENT_TIME > 3.5) && (index == 12)) {
//printf("\n -----------inside rt_resolve: else at %.4lf, I am %i", CURRENT_TIME, index);}
 Packet *rerr = Packet::alloc();
 struct hdr_aodv_error *re = HDR_AODV_ERROR(rerr);
 /* 
  * For now, drop the packet and send error upstream.
  * Now the route errors are broadcast to upstream
  * neighbors - Mahesh 09/11/99
  */	
 
   assert (rt->rt_flags == RTF_DOWN);
   re->DestCount = 0;
   re->unreachable_dst[re->DestCount] = rt->rt_dst;
   re->unreachable_dst_seqno[re->DestCount] = rt->rt_seqno;
   re->DestCount += 1;
#ifdef DEBUG
   fprintf(stderr, "%s: sending RERR...\n", __FUNCTION__);
   fclose(stderr);
#endif
   sendError(rerr, false);

   drop(p, DROP_RTR_NO_ROUTE);
 }

}

void
AODV::rt_purge() {
aodv_rt_entry *rt, *rtn;
double now = CURRENT_TIME;
double delay = 0.0;
#ifdef DEBUG
FILE * stderr;
stderr = fopen("debugging.txt", "a");
#endif
Packet *p;

 for(rt = rtable.head(); rt; rt = rtn) {  // for each rt entry
   rtn = rt->rt_link.le_next;
   if ((rt->rt_flags == RTF_UP) && (rt->rt_expire < now)) {
   // if a valid route has expired, purge all packets from 
   // send buffer and invalidate the route.                    
	assert(rt->rt_hops != INFINITY2);
     while((p = rqueue.deque(rt->rt_dst))) {
#ifdef DEBUG
       fprintf(stderr, "%s: calling drop()\n",__FUNCTION__);
       fclose(stderr);
#endif // DEBUG
       drop(p, DROP_RTR_NO_ROUTE);
     }
     rt->rt_seqno++;
     assert (rt->rt_seqno%2);
     rt_down(rt);
   }
   else if (rt->rt_flags == RTF_UP) {
   // If the route is not expired,
   // and there are packets in the sendbuffer waiting,
   // forward them. This should not be needed, but this extra 
   // check does no harm.
     assert(rt->rt_hops != INFINITY2);
     while((p = rqueue.deque(rt->rt_dst))) {
       forward (rt, p, delay);
       delay += ARP_DELAY;
     }
   } 
   else if (rqueue.find(rt->rt_dst))
   // If the route is down and 
   // if there is a packet for this destination waiting in
   // the sendbuffer, then send out route request. sendRequest
   // will check whether it is time to really send out request
   // or not.
   // This may not be crucial to do it here, as each generated 
   // packet will do a sendRequest anyway.
//printf(" \n Inside rt_purge() at %.4lf, going to call sendRequest...", CURRENT_TIME);
     sendRequest(rt->rt_dst); 	// Modifikasi : Tambahan untuk OF
   }

}

/*
  Packet Reception Routines
*/

void
AODV::recv(Packet *p, Handler*) {
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *ih = HDR_IP(p);

 assert(initialized());
 //assert(p->incoming == 0);
 // XXXXX NOTE: use of incoming flag has been depracated; In order to track direction of pkt flow, direction_ in hdr_cmn is used instead. see packet.h for details.
//------------------------- Modifikasi : Tambahan untuk OF--------------------------------------------
if(index == ih->daddr() && DATA_PACKET(ch->ptype()))
{
	//printf("\ndestination received data packet at time: %.9lf", CURRENT_TIME);
	dFlag = true;
	//return;
} 

if((ih->daddr() == index) || DATA_PACKET(ch->ptype()))	//Modifikasi : Tambahan untuk OF
	{
		//if((index == 5) && (DATA_PACKET(ch->ptype()))) { printf("\ndestination recvd data packet at time: %.4lf", CURRENT_TIME); }
		dFlag = true;
	}

//if(DATA_PACKET(ch->ptype())) { printf("\nNode: _%i _ recvd data packet at time: %.4lf", index, CURRENT_TIME); }

//rt_print(index);		//Modifikasi : Print Routing Table
//art_print(index);		//Modifikasi : Print Alternate Route Table
//nb_print(index);		//Modifikasi : Print Neigbor Table
//----------------------------------------------------------------------------------------
 if(ch->ptype() == PT_AODV) {
   ih->ttl_ -= 1;
   recvAODV(p);
   return;
 }

 //if(ch->ptype() == PT_CBR) {
 //  rt_print(index);	// Untuk menampilkan Routing Table
 //}
	
 /*
  *  Must be a packet I'm originating...
  */
if((ih->saddr() == index) && (ch->num_forwards() == 0)) {

if(ch->ptype() == PT_CBR) {ch->height = -1; nHeight = -1;}	// Modifikasi : Penentuan nilai Height pada Node Source
 /*
  * Add the IP Header.  
  * TCP adds the IP header too, so to avoid setting it twice, we check if
  * this packet is not a TCP or ACK segment.
  */
  if (ch->ptype() != PT_TCP && ch->ptype() != PT_ACK) {
    ch->size() += IP_HDR_LEN;
  }
   // Added by Parag Dadhania && John Novatnack to handle broadcasting
  if ( (u_int32_t)ih->daddr() != IP_BROADCAST) {
    ih->ttl_ = NETWORK_DIAMETER;
  }
}
 /*
  *  I received a packet that I sent.  Probably
  *  a routing loop.
  */
else if(ih->saddr() == index) {
   drop(p, DROP_RTR_ROUTE_LOOP);
   return;
 }
 /*
  *  Packet I'm forwarding...
  */
 else {
 /*
  *  Check the TTL.  If it is zero, then discard.
  */
   if(--ih->ttl_ == 0) {
     drop(p, DROP_RTR_TTL);
     return;
   }

   //if(ch->ptype() == PT_CBR) { nHeight = ch->height + 1; ch->height++; }  // Modifikasi : untuk peningkatan OF height dari  forwarding node

 }
// Added by Parag Dadhania && John Novatnack to handle broadcasting
 if ( (u_int32_t)ih->daddr() != IP_BROADCAST)
 {
   //ch->prev_hop_ = index; 	//Update field prev_hop_ dari hdr_cmn untuk data packets
   rt_resolve(p);
 } 
 else
 { 
  // ch->prev_hop_ = index;	// Mungkin tidak dibutuhkan
   forward((aodv_rt_entry*) 0, p, NO_DELAY);
 }
}


void
AODV::recvAODV(Packet *p) {
 struct hdr_aodv *ah = HDR_AODV(p);

 assert(HDR_IP (p)->sport() == RT_PORT);
 assert(HDR_IP (p)->dport() == RT_PORT);

 /*
  * Incoming Packets.
  */
 switch(ah->ah_type) {

 case AODVTYPE_RREQ:
   recvRequest(p);
   break;

 case AODVTYPE_RREP:
   recvReply(p);
   break;

 case AODVTYPE_RERR:
   recvError(p);
   break;

 case AODVTYPE_HELLO:
   recvHello(p);
   break;

 default:
   fprintf(stderr, "Invalid AODV type (%x)\n", ah->ah_type);
   exit(1);
 }

}

void
AODV::recvRequest(Packet *p) {
struct hdr_ip *ih = HDR_IP(p);
struct hdr_aodv_request *rq = HDR_AODV_REQUEST(p);
aodv_rt_entry *rt;
#ifdef DEBUG
FILE * stderr;
stderr = fopen("debugging.txt", "a");
#endif

  /*
   * Drop if:
   *      - I'm the source
   *      - I recently heard this request.
   */

  if(rq->rq_src == index) {
#ifdef DEBUG
    fprintf(stderr, "%s: got my own REQUEST\n", __FUNCTION__);
#endif // DEBUG
    Packet::free(p);
    return;
  } 

 if (id_lookup(rq->rq_src, rq->rq_bcast_id)) {

#ifdef DEBUG
   fprintf(stderr, "%s: discarding request\n", __FUNCTION__);
#endif // DEBUG
 
   Packet::free(p);
   return;
 }

 /*
  * Cache the broadcast ID
  */
 id_insert(rq->rq_src, rq->rq_bcast_id);



 /* 
  * We are either going to forward the REQUEST or generate a
  * REPLY. Before we do anything, we make sure that the REVERSE
  * route is in the route table.
  */
 aodv_rt_entry *rt0; // rt0 is the reverse route 
   
   rt0 = rtable.rt_lookup(rq->rq_src);
   if(rt0 == 0) { /* if not in the route table */
   // create an entry for the reverse route.
     rt0 = rtable.rt_add(rq->rq_src);
   }
  
   rt0->rt_expire = max(rt0->rt_expire, (CURRENT_TIME + REV_ROUTE_LIFE));

   if ( (rq->rq_src_seqno > rt0->rt_seqno ) ||
    	((rq->rq_src_seqno == rt0->rt_seqno) && 
	 (rq->rq_hop_count < rt0->rt_hops)) ) {
   // If we have a fresher seq no. or lesser #hops for the 
   // same seq no., update the rt entry. Else don't bother.
rt_update(rt0, rq->rq_src_seqno, rq->rq_hop_count, ih->saddr(),
     	       max(rt0->rt_expire, (CURRENT_TIME + REV_ROUTE_LIFE)) );
     if (rt0->rt_req_timeout > 0.0) {
     // Reset the soft state and 
     // Set expiry time to CURRENT_TIME + ACTIVE_ROUTE_TIMEOUT
     // This is because route is used in the forward direction,
     // but only sources get benefited by this change
       rt0->rt_req_cnt = 0;
       rt0->rt_req_timeout = 0.0; 
       rt0->rt_req_last_ttl = rq->rq_hop_count;
       rt0->rt_expire = CURRENT_TIME + ACTIVE_ROUTE_TIMEOUT;
     }

     /* Find out whether any buffered packet can benefit from the 
      * reverse route.
      * May need some change in the following code - Mahesh 09/11/99
      */
     assert (rt0->rt_flags == RTF_UP);
     Packet *buffered_pkt;
     while ((buffered_pkt = rqueue.deque(rt0->rt_dst))) {
       if (rt0 && (rt0->rt_flags == RTF_UP)) {
	assert(rt0->rt_hops != INFINITY2);
         forward(rt0, buffered_pkt, NO_DELAY);
       }
     }
   } 
   // End for putting reverse route in rt table


 /*
  * We have taken care of the reverse route stuff.
  * Now see whether we can send a route reply. 
  */

 rt = rtable.rt_lookup(rq->rq_dst);

 // First check if I am the destination ..

 if(rq->rq_dst == index) {

#ifdef DEBUG
   fprintf(stderr, "%d - %s: destination sending reply\n",index, __FUNCTION__);
#endif // DEBUG

               
   // Just to be safe, I use the max. Somebody may have
   // incremented the dst seqno.
   seqno = max(seqno, rq->rq_dst_seqno)+1;
   if (seqno%2) seqno++;

   sendReply(rq->rq_src,           // IP Destination
             1,                    // Hop Count
             index,                // Dest IP Address
             seqno,                // Dest Sequence Num
             MY_ROUTE_TIMEOUT,     // Lifetime
             rq->rq_timestamp);    // timestamp
 
   Packet::free(p);
 }

 // I am not the destination, but I may have a fresh enough route.

 else if (rt && (rt->rt_hops != INFINITY2) && 
	  	(rt->rt_seqno >= rq->rq_dst_seqno) ) {

   //assert (rt->rt_flags == RTF_UP);
   assert(rq->rq_dst == rt->rt_dst);
   //assert ((rt->rt_seqno%2) == 0);	// is the seqno even?
   sendReply(rq->rq_src,
             rt->rt_hops + 1,
             rq->rq_dst,
             rt->rt_seqno,
	     (u_int32_t) (rt->rt_expire - CURRENT_TIME),
	     //             rt->rt_expire - CURRENT_TIME,
             rq->rq_timestamp);
   // Insert nexthops to RREQ source and RREQ destination in the
   // precursor lists of destination and source respectively
   rt->pc_insert(rt0->rt_nexthop); // nexthop to RREQ source
   rt0->pc_insert(rt->rt_nexthop); // nexthop to RREQ destination

#ifdef RREQ_GRAT_RREP  

   sendReply(rq->rq_dst,
             rq->rq_hop_count,
             rq->rq_src,
             rq->rq_src_seqno,
	     (u_int32_t) (rt->rt_expire - CURRENT_TIME),
	     //             rt->rt_expire - CURRENT_TIME,
             rq->rq_timestamp);
#endif
   
// TODO: send grat RREP to dst if G flag set in RREQ using rq->rq_src_seqno, rq->rq_hop_counT
   
// DONE: Included gratuitous replies to be sent as per IETF aodv draft specification. As of now, G flag has not been dynamically used and is always set or reset in aodv-packet.h --- Anant Utgikar, 09/16/02.

	Packet::free(p);
 }
 /*
  * Can't reply. So forward the  Route Request
  */
 else {
   ih->saddr() = index;
   ih->daddr() = IP_BROADCAST;
   rq->rq_hop_count += 1;
   // Maximum sequence number seen en route
   if (rt) rq->rq_dst_seqno = max(rt->rt_seqno, rq->rq_dst_seqno);
   forward((aodv_rt_entry*) 0, p, DELAY);
 }
#ifdef DEBUG
fclose(stderr);
#endif
}


void
AODV::recvReply(Packet *p) {
//struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *ih = HDR_IP(p);
struct hdr_aodv_reply *rp = HDR_AODV_REPLY(p);
aodv_rt_entry *rt;
char suppress_reply = 0;
double delay = 0.0;
	
#ifdef DEBUG
FILE * stderr;
stderr = fopen("debugging.txt", "a");
 fprintf(stderr, "%d - %s: received a REPLY\n", index, __FUNCTION__);
#endif // DEBUG


 /*
  *  Got a reply. So reset the "soft state" maintained for 
  *  route requests in the request table. We don't really have
  *  have a separate request table. It is just a part of the
  *  routing table itself. 
  */
 // Note that rp_dst is the dest of the data packets, not the
 // the dest of the reply, which is the src of the data packets.

 rt = rtable.rt_lookup(rp->rp_dst);
        
 /*
  *  If I don't have a rt entry to this host... adding
  */
 if(rt == 0) {
   rt = rtable.rt_add(rp->rp_dst);
 }

 /*
  * Add a forward route table entry... here I am following 
  * Perkins-Royer AODV paper almost literally - SRD 5/99
  */

 if ( (rt->rt_seqno < rp->rp_dst_seqno) ||   // newer route 
      ((rt->rt_seqno == rp->rp_dst_seqno) &&  
       (rt->rt_hops > rp->rp_hop_count)) ) { // shorter or better route
	
  // Update the rt entry 
  rt_update(rt, rp->rp_dst_seqno, rp->rp_hop_count,
		rp->rp_src, CURRENT_TIME + rp->rp_lifetime);

  // reset the soft state
  rt->rt_req_cnt = 0;
  rt->rt_req_timeout = 0.0; 
  rt->rt_req_last_ttl = rp->rp_hop_count;
  
if (ih->daddr() == index) { // If I am the original source
  // Update the route discovery latency statistics
  // rp->rp_timestamp is the time of request origination
		
    rt->rt_disc_latency[(unsigned char)rt->hist_indx] = (CURRENT_TIME - rp->rp_timestamp)
                                         / (double) rp->rp_hop_count;
    // increment indx for next time
    rt->hist_indx = (rt->hist_indx + 1) % MAX_HISTORY;
  }	

  /*
   * Send all packets queued in the sendbuffer destined for
   * this destination. 
   * XXX - observe the "second" use of p.
   */
  Packet *buf_pkt;
  while((buf_pkt = rqueue.deque(rt->rt_dst))) {
    if(rt->rt_hops != INFINITY2) {
          assert (rt->rt_flags == RTF_UP);
    // Delay them a little to help ARP. Otherwise ARP 
    // may drop packets. -SRD 5/23/99
      forward(rt, buf_pkt, delay);
      delay += ARP_DELAY;
    }
  }
 }
 else {
  suppress_reply = 1;
 }

 /*
  * If reply is for me, discard it.
  */

if(ih->daddr() == index || suppress_reply) {
   Packet::free(p);
 }
 /*
  * Otherwise, forward the Route Reply.
  */
 else {
 // Find the rt entry
aodv_rt_entry *rt0 = rtable.rt_lookup(ih->daddr());
   // If the rt is up, forward
   if(rt0 && (rt0->rt_hops != INFINITY2)) {
        assert (rt0->rt_flags == RTF_UP);
     rp->rp_hop_count += 1;
     rp->rp_src = index;
     forward(rt0, p, NO_DELAY);
     // Insert the nexthop towards the RREQ source to 
     // the precursor list of the RREQ destination
     rt->pc_insert(rt0->rt_nexthop); // nexthop to RREQ source
     
   }
   else {
   // I don't know how to forward .. drop the reply. 
#ifdef DEBUG
     fprintf(stderr, "%s: dropping Route Reply\n", __FUNCTION__);
     fclose(stderr);
#endif // DEBUG
     drop(p, DROP_RTR_NO_ROUTE);
   }
 }
}

void
AODV::recvError(Packet *p) {
struct hdr_ip *ih = HDR_IP(p);
struct hdr_aodv_error *re = HDR_AODV_ERROR(p);
aodv_rt_entry *rt;
u_int8_t i;
Packet *rerr = Packet::alloc();
struct hdr_aodv_error *nre = HDR_AODV_ERROR(rerr);
#ifdef DEBUG
FILE * stderr;
stderr = fopen("debugging.txt", "a");
#endif

 nre->DestCount = 0;

 for (i=0; i<re->DestCount; i++) {
 // For each unreachable destination
   rt = rtable.rt_lookup(re->unreachable_dst[i]);
   if ( rt && (rt->rt_hops != INFINITY2) &&
	(rt->rt_nexthop == ih->saddr()) &&
     	(rt->rt_seqno <= re->unreachable_dst_seqno[i]) ) {
	assert(rt->rt_flags == RTF_UP);
	assert((rt->rt_seqno%2) == 0); // is the seqno even?
#ifdef DEBUG
     fprintf(stderr, "%s(%f): %d\t(%d\t%u\t%d)\t(%d\t%u\t%d)\n", __FUNCTION__,CURRENT_TIME, index, rt->rt_dst, rt->rt_seqno, rt->rt_nexthop, re->unreachable_dst[i],re->unreachable_dst_seqno[i], ih->saddr());
#endif // DEBUG
     	rt->rt_seqno = re->unreachable_dst_seqno[i];
     	rt_down(rt);

   // Not sure whether this is the right thing to do
   Packet *pkt;
	while((pkt = ifqueue->filter(ih->saddr()))) {
        	drop(pkt, DROP_RTR_MAC_CALLBACK);
     	}

     // if precursor list non-empty add to RERR and delete the precursor list
     	if (!rt->pc_empty()) {
     		nre->unreachable_dst[nre->DestCount] = rt->rt_dst;
     		nre->unreachable_dst_seqno[nre->DestCount] = rt->rt_seqno;
     		nre->DestCount += 1;
		rt->pc_delete();
     	}
   }
 } 

 if (nre->DestCount > 0) {
#ifdef DEBUG
   fprintf(stderr, "%s(%f): %d\t sending RERR...\n", __FUNCTION__, CURRENT_TIME, index);
#endif // DEBUG
   sendError(rerr);
 }
 else {
   Packet::free(rerr);
 }

 Packet::free(p);
#ifdef DEBUG
fclose(stderr);
#endif
}


/*
   Packet Transmission Routines
*/

void
AODV::forward(aodv_rt_entry *rt, Packet *p, double delay) {
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *ih = HDR_IP(p);

 if(ih->ttl_ == 0) {

#ifdef DEBUG
FILE * stderr;
stderr = fopen("debugging.txt", "a");
  fprintf(stderr, "%s: calling drop()\n", __PRETTY_FUNCTION__);
#endif // DEBUG
 
  drop(p, DROP_RTR_TTL);
  return;
 }

 if ( (ch->ptype() != PT_AODV) && (ch->direction() == hdr_cmn::UP) &&
	( (u_int32_t)ih->daddr() == IP_BROADCAST)
		|| (ih->daddr() == here_.addr_) ) {
	dmux_->recv(p,0);
	return;
 }

 if (rt) {
   assert(rt->rt_flags == RTF_UP);
   rt->rt_expire = CURRENT_TIME + ACTIVE_ROUTE_TIMEOUT;
   ch->next_hop_ = rt->rt_nexthop;
   ch->addr_type() = NS_AF_INET;
   ch->direction() = hdr_cmn::DOWN;       //important: change the packet's direction
 }
 else { // if it is a broadcast packet
   // assert(ch->ptype() == PT_AODV); // maybe a diff pkt type like gaf
   assert(ih->daddr() == (nsaddr_t) IP_BROADCAST);
   ch->addr_type() = NS_AF_NONE;
   ch->direction() = hdr_cmn::DOWN;       //important: change the packet's direction
 }

if (ih->daddr() == (nsaddr_t) IP_BROADCAST) {
 // If it is a broadcast packet
   assert(rt == 0);
   if (ch->ptype() == PT_AODV) {
     /*
      *  Jitter the sending of AODV broadcast packets by 10ms
      */
     Scheduler::instance().schedule(target_, p,
      				   0.01 * Random::uniform());
   } else {
     Scheduler::instance().schedule(target_, p, 0.);  // No jitter
   }
 }
 else { // Not a broadcast packet 
ch->prev_hop_ = index; 	// Modifikasi : Update field prev_hop dari hdr_cmn untuk data packets
   if(delay > 0.0) {
     Scheduler::instance().schedule(target_, p, delay);
   }
   else {
   // Not a broadcast packet, no delay, send immediately
     Scheduler::instance().schedule(target_, p, 0.);
   }
 }
#ifdef DEBUG
fclose(stderr);
#endif
}


void
AODV::sendRequest(nsaddr_t dst) {
//printf("\n Inside sendRequest at %f \n", CURRENT_TIME);
// Allocate a RREQ packet 
Packet *p = Packet::alloc();
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *ih = HDR_IP(p);
struct hdr_aodv_request *rq = HDR_AODV_REQUEST(p);
aodv_rt_entry *rt = rtable.rt_lookup(dst);
#ifdef DEBUG
FILE * stderr;
stderr = fopen("debugging.txt", "a");
#endif
 assert(rt);

 /*
  *  Rate limit sending of Route Requests. We are very conservative
  *  about sending out route requests. 
  */

 if (rt->rt_flags == RTF_UP) {
   assert(rt->rt_hops != INFINITY2);
   Packet::free((Packet *)p);
   return;
 }

 if (rt->rt_req_timeout > CURRENT_TIME) {
   Packet::free((Packet *)p);
   return;
 }

 // rt_req_cnt is the no. of times we did network-wide broadcast
 // RREQ_RETRIES is the maximum number we will allow before 
 // going to a long timeout.

 if (rt->rt_req_cnt > RREQ_RETRIES) {
   rt->rt_req_timeout = CURRENT_TIME + MAX_RREQ_TIMEOUT;
   rt->rt_req_cnt = 0;
 Packet *buf_pkt;
   while ((buf_pkt = rqueue.deque(rt->rt_dst))) {
       drop(buf_pkt, DROP_RTR_NO_ROUTE);
   }
   Packet::free((Packet *)p);
   return;
 }

#ifdef DEBUG
   fprintf(stderr, "(%2d) - %2d sending Route Request, dst: %d\n",
                    ++route_request, index, rt->rt_dst);
#endif // DEBUG

 // Determine the TTL to be used this time. 
 // Dynamic TTL evaluation - SRD

 rt->rt_req_last_ttl = max(rt->rt_req_last_ttl,rt->rt_last_hop_count);

 if (0 == rt->rt_req_last_ttl) {
 // first time query broadcast
   ih->ttl_ = TTL_START;
 }
 else {
 // Expanding ring search.
   if (rt->rt_req_last_ttl < TTL_THRESHOLD)
     ih->ttl_ = rt->rt_req_last_ttl + TTL_INCREMENT;
   else {
   // network-wide broadcast
     ih->ttl_ = NETWORK_DIAMETER;
     rt->rt_req_cnt += 1;
   }
 }

 // remember the TTL used  for the next time
 rt->rt_req_last_ttl = ih->ttl_;

 // PerHopTime is the roundtrip time per hop for route requests.
 // The factor 2.0 is just to be safe .. SRD 5/22/99
 // Also note that we are making timeouts to be larger if we have 
 // done network wide broadcast before. 

 rt->rt_req_timeout = 2.0 * (double) ih->ttl_ * PerHopTime(rt); 
 if (rt->rt_req_cnt > 0)
   rt->rt_req_timeout *= rt->rt_req_cnt;
 rt->rt_req_timeout += CURRENT_TIME;

 // Don't let the timeout to be too large, however .. SRD 6/8/99
 if (rt->rt_req_timeout > CURRENT_TIME + MAX_RREQ_TIMEOUT)
   rt->rt_req_timeout = CURRENT_TIME + MAX_RREQ_TIMEOUT;
 rt->rt_expire = 0;

#ifdef DEBUG
 fprintf(stderr, "(%2d) - %2d sending Route Request, dst: %d, tout %f ms\n",
	         ++route_request, 
		 index, rt->rt_dst, 
		 rt->rt_req_timeout - CURRENT_TIME);
#endif	// DEBUG
	

 // Fill out the RREQ packet 
 // ch->uid() = 0;
 ch->ptype() = PT_AODV;
 ch->size() = IP_HDR_LEN + rq->size();
 ch->iface() = -2;
 ch->error() = 0;
 ch->addr_type() = NS_AF_NONE;
 ch->prev_hop_ = index;          // AODV hack

 ih->saddr() = index;
 ih->daddr() = IP_BROADCAST;
 ih->sport() = RT_PORT;
 ih->dport() = RT_PORT;

 // Fill up some more fields. 
 rq->rq_type = AODVTYPE_RREQ;
 rq->rq_hop_count = 1;
 rq->rq_bcast_id = bid++;
 rq->rq_dst = dst;
 rq->rq_dst_seqno = (rt ? rt->rt_seqno : 0);
 rq->rq_src = index;
 seqno += 2;
 assert ((seqno%2) == 0);
 rq->rq_src_seqno = seqno;
 rq->rq_timestamp = CURRENT_TIME;

 Scheduler::instance().schedule(target_, p, 0.);
#ifdef DEBUG
fclose(stderr);
#endif
}

void
AODV::sendReply(nsaddr_t ipdst, u_int32_t hop_count, nsaddr_t rpdst,
                u_int32_t rpseq, u_int32_t lifetime, double timestamp) {
Packet *p = Packet::alloc();
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *ih = HDR_IP(p);
struct hdr_aodv_reply *rp = HDR_AODV_REPLY(p);
aodv_rt_entry *rt = rtable.rt_lookup(ipdst);

#ifdef DEBUG
FILE * stderr;
stderr = fopen("debugging.txt", "a");
fprintf(stderr, "sending Reply from %d at %.2f\n", index, Scheduler::instance().clock());
fclose(stderr);
#endif // DEBUG
 assert(rt);

 rp->rp_type = AODVTYPE_RREP;
 //rp->rp_flags = 0x00;
 rp->rp_hop_count = hop_count;
 rp->rp_dst = rpdst;
 rp->rp_dst_seqno = rpseq;
 rp->rp_src = index;
 rp->rp_lifetime = lifetime;
 rp->rp_timestamp = timestamp;
   
 // ch->uid() = 0;
 ch->ptype() = PT_AODV;
 ch->size() = IP_HDR_LEN + rp->size();
 ch->iface() = -2;
 ch->error() = 0;
 ch->addr_type() = NS_AF_INET;
 ch->next_hop_ = rt->rt_nexthop;
 ch->prev_hop_ = index;          // AODV hack
 ch->direction() = hdr_cmn::DOWN;

 ih->saddr() = index;
 ih->daddr() = ipdst;
 ih->sport() = RT_PORT;
 ih->dport() = RT_PORT;
 ih->ttl_ = NETWORK_DIAMETER;

 Scheduler::instance().schedule(target_, p, 0.);

}

void
AODV::sendError(Packet *p, bool jitter) {
//printf("\n Inside sendError at %f \n", CURRENT_TIME);
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *ih = HDR_IP(p);
struct hdr_aodv_error *re = HDR_AODV_ERROR(p);
    
#ifdef ERROR
fprintf(stderr, "sending Error from %d at %.2f\n", index, Scheduler::instance().clock());
#endif // DEBUG

 re->re_type = AODVTYPE_RERR;
 //re->reserved[0] = 0x00; re->reserved[1] = 0x00;
 // DestCount and list of unreachable destinations are already filled

 // ch->uid() = 0;
 ch->ptype() = PT_AODV;
 ch->size() = IP_HDR_LEN + re->size();
 ch->iface() = -2;
 ch->error() = 0;
 ch->addr_type() = NS_AF_NONE;
 ch->next_hop_ = 0;
 ch->prev_hop_ = index;          // AODV hack
 ch->direction() = hdr_cmn::DOWN;       //important: change the packet's direction

 ih->saddr() = index;
 ih->daddr() = IP_BROADCAST;
 ih->sport() = RT_PORT;
 ih->dport() = RT_PORT;
 ih->ttl_ = 1;

 // Do we need any jitter? Yes
 if (jitter)
 	Scheduler::instance().schedule(target_, p, 0.01*Random::uniform());
 else
 	Scheduler::instance().schedule(target_, p, 0.0);

}


/*
   Neighbor Management Functions
*/

void
AODV::sendHello() {
Packet *p = Packet::alloc();
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *ih = HDR_IP(p);
struct hdr_aodv_reply *rh = HDR_AODV_REPLY(p);

#ifdef DEBUG
FILE * stderr;
stderr = fopen("debugging.txt", "a");
fprintf(stderr, "sending Hello from %d at %.2f, height: %.1lf\n", index, Scheduler::instance().clock(), nHeight);
fclose(stderr);
#endif // DEBUG
//printf("inside sendHello, I am %i, my height is %.1lf\t", index, nHeight);

 rh->rp_type = AODVTYPE_HELLO;
 //rh->rp_flags = 0x00;
 rh->rp_hop_count = 1;
 rh->rp_dst = index;
 rh->rp_dst_seqno = seqno;
 rh->rp_lifetime = (1 + ALLOWED_HELLO_LOSS) * HELLO_INTERVAL;
//printf("Inside sendHello(), I am %i, my current OF is %i\n", index, OF);
 rh->rp_OF = OF;	// Modifikasi : OF pada Hello Packet
 aodv_art_entry *art;
 double ht;
 u_int8_t count = 1;
//--------------------------- Modifikasi : Perhitungan OF ------------------------------------------	
if(artable.head())
{	
 for (art=artable.head(), ht = art->art_height;art; art = art->art_link.le_next)
 {
	if(art->art_height != 0)
	{
		if(art->art_link.le_next)
		{
			if((art->art_dst == art->art_link.le_next->art_dst) && (art->art_link.le_next->art_height != 0))
			{
				ht += art->art_link.le_next->art_height;
				count++;
			}
		}
	}
	else	//heigh values in the ART entries not yet updated
	{
		ht = nHeight;
		count = 1;
		break;
	}		
 }
}
  if(count == 1) //no averaging required as only one ART entry
  {
	ht = nHeight;	
  }

  ch->height = ht/count;	// Modifikasi : Penambahan variabel height pada Hello Packet
//printf("I am sending height = %.1lf at %.4lf \n", ch->height, CURRENT_TIME);
//--------------------------------------------------------------------------------------------- 
 // ch->uid() = 0;
 ch->ptype() = PT_AODV;
 ch->size() = IP_HDR_LEN + rh->size();
 ch->iface() = -2;
 ch->error() = 0;
 ch->addr_type() = NS_AF_NONE;
 ch->prev_hop_ = index;          // AODV hack

 ih->saddr() = index;
 ih->daddr() = IP_BROADCAST;
 ih->sport() = RT_PORT;
 ih->dport() = RT_PORT;
 ih->ttl_ = 1;

 Scheduler::instance().schedule(target_, p, 0.0);	
}

void
AODV::updateOF()
{
//printf("\n Inside updateOF() \n");
	aodv_art_entry *art;
	int countEntries = 1;
	if(artable.head())
	{
 		for(art=artable.head(); art; art = art->art_link.le_next)		
		{
			if(art->art_link.le_next)
			{
				if(art->art_dst == art->art_link.le_next->art_dst)	
				{//not good when the dst fields of the entries are jumbled up, (but seems it works fine, not sure)
					countEntries++;
					//printf("\n Inside updateOF(): countEntries = %i", countEntries);
				}
			}
		}
		//if
		//{
			OF = countEntries;
		//}
	}
	//else
	//{
	//	OF = 1;	//No entries,  default OF
	//}	
#ifdef DEBUG
FILE * stderr;
stderr = fopen("debugging.txt", "a");
fprintf(stderr, "Update OF berjalan. \n");
fclose(stderr);
#endif
}


void
AODV::recvHello(Packet *p) {
//struct hdr_ip *ih = HDR_IP(p);
struct hdr_aodv_reply *rp = HDR_AODV_REPLY(p);
struct hdr_cmn *ch = HDR_CMN(p);
AODV_Neighbor *nb;
aodv_art_entry *art;

 nb = nb_lookup(rp->rp_dst);
 if(nb == 0) {
   nb_insert(rp->rp_dst, rp->rp_OF, rp->rp_dst_seqno, ch->height);	// Modifikasi : Untuk OF
	
	updateOF();	// Modifikasi : untuk OF
 }
 else {
   nb->nb_expire = CURRENT_TIME +
                   (1.5 * ALLOWED_HELLO_LOSS * HELLO_INTERVAL);
   nb->nb_OF = rp->rp_OF;	// Modifikasi : Update nilai OF neighbor pada Neighbor Table
 }

//------------------------------------Modifikasi : Tambahan untuk OF------------------------------------------
 art = art_lookup(rp->rp_dst);
 if(art != 0)
 {
    art->art_expire = CURRENT_TIME + 
                    (1.5 * ALLOWED_HELLO_LOSS * HELLO_INTERVAL);
 }
//------------------------------------Modifikasi : Tambahan untuk OF------------------------------------------

 Packet::free(p);
}

void
AODV::nb_insert(nsaddr_t id, u_int8_t recvdOF, u_int8_t seqNum, double nHt) {
AODV_Neighbor *nb = new AODV_Neighbor(id);

 assert(nb);
 nb->nb_expire = CURRENT_TIME +
                (1.5 * ALLOWED_HELLO_LOSS * HELLO_INTERVAL);
 nb->nb_OF = recvdOF;	// Modifikasi : Penambahan nilai OF pada neighbor entry baru dari hello packet yang diterima
 nb->nb_seqno = seqNum;	// Sequence Number
 nb->nb_height = nHt;	//Modifikasi : Penambahan infromasi Height pada Neigbor Table
 LIST_INSERT_HEAD(&nbhead, nb, nb_link);
 seqno += 2;             // set of neighbors changed
 assert ((seqno%2) == 0);
}


AODV_Neighbor*
AODV::nb_lookup(nsaddr_t id) {
AODV_Neighbor *nb = nbhead.lh_first;

 for(; nb; nb = nb->nb_link.le_next) {
   if(nb->nb_addr == id) break;
 }
 return nb;
}

u_int8_t
AODV::nb_count() {
AODV_Neighbor *nb = nbhead.lh_first;

 u_int8_t count = 0;
 for(; nb; nb = nb->nb_link.le_next) {
   count++;
 }
 return count;
}


//------------------------------------Modifikasi : ART Lookup------------------------------------------

aodv_art_entry *
AODV::art_lookup(nsaddr_t id) {
aodv_art_entry *art;

 for(art=artable.head();art; art = art->art_link.le_next) {
   if(art->art_nexthop == id) break;
 }
 return art;
}

/*
 * Called when we receive *explicit* notification that a Neighbor
 * is no longer reachable.
 */
void
AODV::nb_delete(nsaddr_t id) {
AODV_Neighbor *nb = nbhead.lh_first;

 log_link_del(id);
 seqno += 2;     // Set of neighbors changed
 assert ((seqno%2) == 0);

 for(; nb; nb = nb->nb_link.le_next) {
   if(nb->nb_addr == id) {
     LIST_REMOVE(nb,nb_link);
     delete nb;
     break;
   }
 }
//printf("\ninside nb_delete, calling handle_link_failure, at %f++++++++++++++++++++++++++++++", CURRENT_TIME);
 handle_link_failure(id);

}

/*
 *Modifikasi
 *
 *Called when we receive *explicit* notification that a Neighbor
 * is no longer reachable.
 */
void
AODV::art_delete(nsaddr_t id) {
aodv_art_entry *art;

 log_link_del(id);

 for(art=artable.head();art; art = art->art_link.le_next) {
   if(art->art_nexthop == id) {
     LIST_REMOVE(art,art_link);
     delete art;
     break;
   }
 }

 //handle_link_failure(id);

}

/*
 * Purges all timed-out Neighbor Entries - runs every
 * HELLO_INTERVAL * 1.5 seconds.
 */
void
AODV::nb_purge() {
AODV_Neighbor *nb = nbhead.lh_first;
AODV_Neighbor *nbn;
double now = CURRENT_TIME;

 for(; nb; nb = nbn) {
   nbn = nb->nb_link.le_next;
   if(nb->nb_expire <= now) {
//printf("inside nb_purge, calling nb_delete at %f+++++++++++++----------------------", CURRENT_TIME);
     nb_delete(nb->nb_addr);
   }
 }

}


/*
 *Modifikasi
 * 
 *Purges all timed-out ART Entries - runs every
 * HELLO_INTERVAL * 1.5 seconds.
 */
void
AODV::art_purge() {
aodv_art_entry *art = artable.head();
aodv_art_entry *artn;
double now = CURRENT_TIME;

 for(; art; art = artn) {
   artn = art->art_link.le_next;
   if(art->art_expire <= now) {
     art_delete(art->art_nexthop);
   }
 }

}